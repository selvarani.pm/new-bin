READ ME FOR API BIN's
--------------------
Language Used : Java - REST ASSURED
Framework : Cucumber

HOW TO RUN API Framework:
1. Import the project to Eclipse 
2. Go to src/main/java --> com.org.runner
3. Open Runner.java 
4. Run as Junit

HOW TO RUN FEATURE:
1. Go to src/test/java  --> respurces.feature
2. Open APITestBin.feature
3. Run as Cucumber feature.

HOW TO PROVIDE/CHANGE API PATH:
1. Go to src/main/java --> ApplicationAPIKeys.java

HOW TO PROVIDE/CHANGE INPUT DATA:
1. Go to src/main/java --> ApplicationInputData.java