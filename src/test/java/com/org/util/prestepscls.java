package com.org.util;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.junit.Assert;

import application.lib.ApplicationAPIKeys;
import application.lib.ApplicationInputData;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;



public class prestepscls {
	static Logger logger = Logger.getLogger(prestepscls.class.getName());
	static String id = null; 
	static Response response=null;

	public String presteps() 
	{
		if (id == null) {
			logger.info("Create Bin Scenarios started");
	 JSONObject json=new JSONObject();
	 json.put(ApplicationInputData.cbdata1,ApplicationInputData.cbdata2);
	  
	  response=RestAssured.given()
				.header(ApplicationAPIKeys.MasterHeaderKey,ApplicationAPIKeys.getMasterKey())
				.header(ApplicationAPIKeys.BinPrivateKey,ApplicationAPIKeys.getupdateBinPrivateKeyValue())
				.contentType(ApplicationAPIKeys.ContentType)
				.body(json.toString())
				.post();
	  
		JsonPath js=response.jsonPath();
		id=js.get(ApplicationAPIKeys.cbid);
		logger.info(response.getStatusCode());
		 logger.info(response.getBody().asString());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 logger.info("Create Bin Scenarios Completed");
		 
		}
		return id;
	}
	
}
