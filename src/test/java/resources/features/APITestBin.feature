@tag
Feature: Change Privacy Bin with put method

  @tag1
  Scenario: AReading a Bin
    Given the ReadBin URl
    When I send a readget request
    Then verify the Bin is read
    

  @tag1
  Scenario: BChange Privacy Bin
    Given the CPendpoint URl
    When I send a chnageprivacyput request
    Then verify the changeprivacyBin is updated
    And verify CP status code should be success
    
    @tag1
  Scenario: CVersion count
    Given the versioncount URl
    When I send a countget request
    Then verify the count
    
    
  @tag1
  Scenario: DUpdate Bin
    Given the updateBin URl
    When I send a updateput request
    Then verify the updateBin is updated

  @tag1
  Scenario: EDeleteBin
    Given the DeleteBin endpoint URl
    When I send a Delete request
    Then verify the Bin is Deleted
    

  
    
