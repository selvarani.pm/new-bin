package resources.stepdefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.RestAssured;

public class HookUps {
	 static String id=null;
	
	 @Before
	public void GenerateID() {
		System.out.println("Feature Test is in progress");
		String baseURI="https://api.jsonbin.io/v3/b";
		RestAssured.baseURI=baseURI;
 }
	
	@After
	public void TestCLose() {
		System.out.println("Test case is executed succesfully");
		System.out.println("***************************************\n\n\n\n\n\n");
		
	}

}
