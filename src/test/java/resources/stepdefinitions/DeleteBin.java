package resources.stepdefinitions;


import org.apache.log4j.Logger;
import org.junit.Assert;
import com.org.util.prestepscls;
import application.lib.ApplicationAPIKeys;
import cucumber.api.java.en.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;


public class DeleteBin {
	static Logger logger = Logger.getLogger(DeleteBin.class.getName());
	static Response response=null;
	static String basePath=null;
	static String id=null;
	static String js=null;
	static String binid=null;
	
	@Given("^the DeleteBin endpoint URl$")
	public void the_DeleteBin_endpoint_URl() throws Throwable {
		logger.info("Delete Bin Scenarios started");
		prestepscls p = new prestepscls();
		String id = p.presteps();
		RestAssured.basePath=id;
	}

	@When("^I send a Delete request$")
	public void i_send_a_Delete_request() throws Throwable {
		 response=RestAssured.given()
					.header(ApplicationAPIKeys.MasterHeaderKey,ApplicationAPIKeys.getMasterKey())
					.delete(); 
	}


	@Then("^verify the Bin is Deleted$")
	public void verify_the_Bin_is_Deleted() throws Throwable {
		logger.info(response.getStatusCode());
		logger.info(response.getBody().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
		logger.info("Delete Bin Scenarios Completed");
 		 		
	}
 		
	}



