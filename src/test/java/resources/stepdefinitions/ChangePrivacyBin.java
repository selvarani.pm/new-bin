package resources.stepdefinitions;


import org.apache.log4j.Logger;
import org.junit.Assert;
import org.json.JSONObject;
import com.org.util.prestepscls;
import application.lib.ApplicationAPIKeys;
import cucumber.api.java.en.*;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;



public class ChangePrivacyBin {
	static Logger logger = Logger.getLogger(ChangePrivacyBin.class.getName());
	static Response response=null;
	static String id=null;
	static Boolean privacy=null;
	static String finalpath=null;
	static String js=null;
	
	
	@Given("^the CPendpoint URl$")
	public void the_endpoint_URl() throws Throwable {
		logger.info("Change Bin Scenarios started");
		prestepscls p = new prestepscls();
		String id = p.presteps();
		RestAssured.basePath=id+ApplicationAPIKeys.changeprivacy;
	}

	@When("^I send a chnageprivacyput request$")
	public void i_send_a_chnageprivacyput_request() throws Throwable {
		JSONObject json=new JSONObject();
		  
		 response=RestAssured.given()
					.header(ApplicationAPIKeys.MasterHeaderKey,ApplicationAPIKeys.getMasterKey())
					.header(ApplicationAPIKeys.BinPrivateKey,ApplicationAPIKeys.getBinPrivateKeyValue())
					.contentType(ApplicationAPIKeys.ContentType)
					.body(json.toString())
					.put();
		
	}


	@Then("^verify the changeprivacyBin is updated$")
	public void verify_the_changeprivacyBin_is_updated() throws Throwable {
		JsonPath js=response.jsonPath();
		privacy=js.get(ApplicationAPIKeys.getmetadatValue());
 		 		
	}

	 @Then("^verify CP status code should be success$")
	public void verify_CP_status_code_should_be_success() throws Throwable {
		 logger.info(response.getStatusCode());
		 logger.info(response.getBody().asString());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 logger.info("Change Bin Scenarios Completed");
	} 

}
