package resources.stepdefinitions;

import org.apache.log4j.Logger;
import org.junit.Assert;

import com.org.util.prestepscls;
import application.lib.ApplicationAPIKeys;
import cucumber.api.java.en.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;


public class ReadBin {
	static Logger logger = Logger.getLogger(ReadBin.class.getName());
	static Response response=null;
	static String basePath=null;
	static String id=null;
	
	@Given("^the ReadBin URl$")
	public void the_ReadBin_URl() throws Throwable {
		
		prestepscls p = new prestepscls();
		String id = p.presteps();
		RestAssured.basePath=id;
	}

	@When("^I send a readget request$")
	public void i_send_a_readget_request() throws Throwable {
		
		  response=RestAssured.given()
					.header(ApplicationAPIKeys.MasterHeaderKey,ApplicationAPIKeys.getMasterKey())
					.get();
		  
	}


	@Then("^verify the Bin is read$")
	public void verify_the_Bin_is_read() throws Throwable {
		logger.info("Read Bin Scenarios started");
		logger.info(response.getStatusCode());
		 logger.info(response.getBody().asString());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 logger.info("Read Bin Scenarios Completed");
 	 		
	}

   }
