package resources.stepdefinitions;

import org.apache.log4j.Logger;
import org.junit.Assert;
import com.org.util.prestepscls;
import application.lib.ApplicationAPIKeys;
import cucumber.api.java.en.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;


public class Versioncount {
	static Logger logger = Logger.getLogger(Versioncount.class.getName());
	static Response response=null;
	static String basePath=null;
	static String id=null;
	
	@Given("^the versioncount URl$")
	public void the_endpoint_URl() throws Throwable {
		logger.info("Version Count Scenarios started");
		prestepscls p = new prestepscls();
		String id = p.presteps();
		RestAssured.basePath=id+ApplicationAPIKeys.versioncount;
		
	}

	@When("^I send a countget request$")
	public void i_send_a_get_request() throws Throwable {
		 response=RestAssured.given()
				.header(ApplicationAPIKeys.MasterHeaderKey,ApplicationAPIKeys.getMasterKey())
				.header(ApplicationAPIKeys.BinPrivateKey,ApplicationAPIKeys.getBinPrivateKeyValue())
				.contentType(ApplicationAPIKeys.ContentType)
				.get();
	}


	@Then("^verify the count$")
	public void verify_the_count() throws Throwable {
		
 		logger.info(response.getStatusCode());
		 logger.info(response.getBody().asString());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 logger.info("Version Count Scenarios Completed");
	 		
	}

}