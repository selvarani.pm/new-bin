package resources.stepdefinitions;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.junit.Assert;
import com.org.util.prestepscls;
import application.lib.ApplicationAPIKeys;
import application.lib.ApplicationInputData;
import cucumber.api.java.en.*;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class UpdateBin {
	static Logger logger = Logger.getLogger(UpdateBin.class.getName());
	static Response response=null;
	static String basePath=null;
	static Boolean privacy=null;
	
	@Given("^the updateBin URl$")
	public void the_updateBin_URl() throws Throwable {
		logger.info("Update Bin Scenarios started");
		prestepscls p = new prestepscls();
		String id = p.presteps();
		RestAssured.basePath=id;
		
	}

	@When("^I send a updateput request$")
	public void i_send_a_updateput_request() throws Throwable {
		  JSONObject json=new JSONObject();
		  json.put(ApplicationInputData.putdata1,ApplicationInputData.putdata2);
		 
		  response=RestAssured.given()
					.header(ApplicationAPIKeys.MasterHeaderKey,ApplicationAPIKeys.getMasterKey())
					.header(ApplicationAPIKeys.BinPrivateKey,ApplicationAPIKeys.getupdateBinPrivateKeyValue())
					.contentType(ApplicationAPIKeys.ContentType)
					.body(json.toString())
					.put();
	}


	@Then("^verify the updateBin is updated$")
	public void verify_the_updateBin_is_updated() throws Throwable {
		JsonPath js=response.jsonPath();
		privacy=js.get(ApplicationAPIKeys.getmetadatValue());
 		logger.info(privacy);
 		 logger.info(response.getStatusCode());
		 logger.info(response.getBody().asString());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 logger.info("Update Bin Scenarios Completed");
 		 		
	}

	
}
