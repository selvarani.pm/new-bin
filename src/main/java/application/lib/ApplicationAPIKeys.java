package application.lib;

public class ApplicationAPIKeys {
	
	public static String MasterHeaderKey="X-Master-Key";
		private static String MasterHeaderKeyValue="$2b$10$dlREyBjuOSWligP1B6ZJ5OiRtk8MHcoBEdnWl2Skoc.CO7HQlw4xW";
		public static String BinPrivateKey="X-Bin-Private";
		private static String BinPrivateKeyValue="true";
		private static String updatebinvalue="false";
		public static String ContentType="application/json";
		public static String privacy="metadata.private";
		public static String versioncount="/versions/count";
		public static String changeprivacy="/meta/privacy";
		public static String cbid="metadata.id";
		
	
	public static String getMasterKey(){
		return MasterHeaderKeyValue;
	}
	
	public static String getBinPrivateKeyValue(){
		return BinPrivateKeyValue;
	}
	public static String getupdateBinPrivateKeyValue(){
		return updatebinvalue;
	}
	public static String getmetadatValue(){
		return privacy;
	}
	public static String getversioncountValue(){
		return versioncount;
	}
	public static String getcprivacyurlValue(){
		return changeprivacy;
	}
	public static String getcbidValue(){
		return cbid;
	}
}
